# SDL2 Moving balls

The app generates balls with *random* initial position, color, radius
and velocity. The balls *bounce* when touching the app window border. When
*swallowing* option is on, big balls eat smaller ones.

A ball can be *selected* using keyboard (`n` key) or a `left mouse click`.

A selected ball can be:

- *moved* towards the mouse pointer with a `left click`
- *positioned* elsewhere  with a `right click`
- *deleted* with `d`
- *moved*  left, down, up or left using the `vim` keys: `h`, `j`, `k` and `l`

## To do

- [X] Movement of the balls adapt to window changing dimensions event
- [X] Add random ball at a keypress
- [X] Select and deselect a ball with the mouse
- [X] Navigate balls with a key press
- [X] Delete selected ball with a key press
- [ ] Select a group of balls with the mouse
- [X] Change direction of the selected ball
- [X] Implement *swallowing*
- [X] Implement *acceleration*
- [X] Implement *gravity*
- [X] Implement *friction*
- [X] Implement *wind* from left or/and right
- [X] Implement *pause*
- [ ] Implement *command line arguments*
- [X] Use TTF fonts to print info in window

## Screenshot

![](./screenshot.png)

## Usage

### Build and run

The app was tested and works on:

- `Debian Linux`
- `Arch Linux`
- `FreeBSD`

```console
make && ./balls
```

## Keyboard action

- Press `q` to quit
- Press `a` to add a random ball
- Press `d` to delete the selected ball
- Press `n` to select the next ball
- Press `s` to toggle swallowing *on* or *off*
- Press `g` to toggle gravity *on* or *off*
- Press `f` to toggle friction *on* or *off*
- Press `w` to toggle wind *on* or *off*
- Press `p` to toggle pause *on* or *off*
- Press `i` to toggle the status line *on* or *off*
- Press `h` to move ball *left*
- Press `l` to move ball *right*
- Press `j` to move ball *down*
- Press `k` to move ball *up*

## Mouse action

- `Left Click` **inside a ball** selects it
- `Left Click` **outside a ball** changes the direction of the selected ball or
  else of all the balls towards the mouse pointer
- `Right Click` to move the selected ball at mouse position
