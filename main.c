#include<string.h>
#include<SDL2/SDL2_gfxPrimitives.h>
#include "SDL2_template.h"
#include "SDL2_fonts.h"
#include "SDL2_ball.h"
#include "utils.h"

#define INITIAL_NUM_BALLS 5
#define MAX_NUM_BALLS 50
#define VELOCITY_MAX 10
#define VELOCITY_FRACTION 0.5
#define WIND_VELOCITY_MAX 3
#define RAND_RADIUS_MIN 10
#define RAND_RADIUS_MAX 100
#define RADIUS_MAX (MIN(width,heigth)/4)
#define BAD_INDEX MAX_NUM_BALLS
#define STRING_LEN_MAX 255

bool swallow = false;
bool gravity_on = false;
bool friction_on = false;
bool wind_on  = false;
bool pause = false;
bool status_on = false;

size_t balls_num = INITIAL_NUM_BALLS;
size_t selected_ball_index = BAD_INDEX;
uint32_t selected_border_color;
float fr_c = 0.01f;//friction constant
vec2_t gravity = {.x=0, .y=0.2};
vec2_t wind = {.x=0, .y=0};

ball_t ball[MAX_NUM_BALLS];

char status[STRING_LEN_MAX];

font_t f;

void SetBorderColor(SDL_Color color);
void GenerateRandomBall(ball_t* b);
void GenerateBalls();
void GenerateWindVector(vec2_t* wind);
void AddBall();
void DrawBall(ball_t* C);
void DeleteBall(size_t i);
void DeleteSelectedBall();
size_t BigBallContainerIndex(size_t i);
void SetBorderForSelected(ball_t* b);
void RemoveMarker(ball_t* b);
bool SelectBall(int x,int y);
void SelectNextBall();
void MoveSelectedBall(int x,int y);
void ShowStatusLine();
void GoUp(ball_t* b);
void GoDown(ball_t* b);
void GoLeft(ball_t* b);
void GoRight(ball_t* b);

int main()
{
    SetWidthHeigth(800,600);
    SetTitle("SDL2 moving balls");
    SetBackground(&black);
    SetBorderColor(white);

    GenerateBalls();

    InitGraph();
    atexit(CloseGraph);

    FontInit(&f,"font.ttf",16);
    atexit(CloseFonts);

    Loop();

    FontCleanup(&f);

    return 0;
}

// Definitions of functions from SDL2_template.h

void Draw()
{
    if(status_on) ShowStatusLine();

    size_t i = 0;
    while(i < balls_num)
    {
        if(!swallow)
            DrawBall(&ball[i]);
        else
        {
            size_t index = BigBallContainerIndex(i);
            if(index == BAD_INDEX)
                DrawBall(&ball[i]);
            else
            {
                ball[index].rad += ball[i].rad;
                if(ball[index].rad > RADIUS_MAX)
                    ball[index].rad = RADIUS_MAX;
                if(selected_ball_index == i)
                    selected_ball_index = BAD_INDEX;
                DrawBall(&ball[index]);
                DeleteBall(i);
                continue;
            }
        }
        ++i;
    }
}

void HandleKeyPress(SDL_Event e)
{
    if(e.key.keysym.sym == SDLK_s)
    {
        if(swallow)
            swallow = false;
        else
            swallow = true;
    }
    else if(e.key.keysym.sym == SDLK_a)
    {
        AddBall();
    }
    else if(e.key.keysym.sym == SDLK_d)
    {
        DeleteSelectedBall();
    }
    else if(e.key.keysym.sym == SDLK_n)
    {
        SelectNextBall();
    }
    else if(e.key.keysym.sym == SDLK_g)
    {
        if(gravity_on)
            gravity_on = false;
        else
            gravity_on = true;
    }
    else if(e.key.keysym.sym == SDLK_f)
    {
        if(friction_on)
            friction_on = false;
        else
            friction_on = true;
    }
    else if(e.key.keysym.sym == SDLK_w)
    {
        if(wind_on)
            wind_on = false;
        else
        {
            wind_on = true;
            GenerateWindVector(&wind);
        }
    }
    else if(e.key.keysym.sym == SDLK_p)
    {
        if(pause)
            pause = false;
        else
            pause = true;
    }
    else if(e.key.keysym.sym == SDLK_i)
    {
        if(status_on)
            status_on = false;
        else
            status_on = true;
    }
    else if(e.key.keysym.sym == SDLK_k)
    {
        if(selected_ball_index == BAD_INDEX) return;
        GoUp(&ball[selected_ball_index]);
    }
    else if(e.key.keysym.sym == SDLK_j)
    {
        if(selected_ball_index == BAD_INDEX) return;
        GoDown(&ball[selected_ball_index]);
    }
    else if(e.key.keysym.sym == SDLK_l)
    {
        if(selected_ball_index == BAD_INDEX) return;
        GoRight(&ball[selected_ball_index]);
    }
    else if(e.key.keysym.sym == SDLK_h)
    {
        if(selected_ball_index == BAD_INDEX) return;
        GoLeft(&ball[selected_ball_index]);
    }
}

void HandleMouse(SDL_Event e)
{
    int x,y;
    if(e.button.button == SDL_BUTTON_LEFT)
    {
        SDL_GetMouseState(&x,&y);
        if(!SelectBall(x,y))
        {
            if(selected_ball_index != BAD_INDEX)
            {
                // Move the current ball towards (x,y)
                MoveBallTowardsPoint(&ball[selected_ball_index],x,y);
            }
            else
            {
                // Move all the balls towards (x,y)
                for(size_t i=0;i<balls_num;++i)
                {
                    MoveBallTowardsPoint(&ball[i],x,y);
                }
            }
        }
    }
    else if(e.button.button == SDL_BUTTON_RIGHT)
    {
        SDL_GetMouseState(&x,&y);
        MoveSelectedBall(x,y);
    }
}

// Specific app functions

void DrawBall(ball_t* C)
{
    if(!pause)
    {
        if(friction_on)
        {
            vec2_t friction = C->velocity;
            Normalize(&friction);
            Multiply(&friction,-1);
            Multiply(&friction,fr_c);
            ApplyForce(C,&friction);
        }

        if(gravity_on)
        {
            ApplyForce(C,&gravity);
        }

        if(wind_on)
        {
            ApplyForce(C,&wind);
        }

        Accelerate(C);

        Add(&C->center,&C->velocity);

        if(C->center.x+C->rad > width)
        {
            C->velocity.x*=-1;
            C->center.x=width-C->rad;
        }

        if(C->center.x-C->rad < 0)
        {
            C->velocity.x*=-1;
            C->center.x=C->rad;
        }

        if(C->center.y+C->rad>heigth)
        {
            C->velocity.y*=-1;
            C->center.y=heigth-C->rad;
        }

        if(C->center.y-C->rad<0)
        {
            C->velocity.y*=-1;
            C->center.y=C->rad;
        }
    }

    if(filledCircleColor(renderer,C->center.x,C->center.y,C->rad,C->color)<0)
    {
        fprintf(stderr,"Filled circle draw error: center=(%f,%f) radius=%d\n",
                C->center.x,C->center.y,C->rad);
    }

    if(C->color != C->border_color)
    {
        circleColor(renderer,C->center.x,C->center.y,C->rad,C->border_color);
        hlineColor(renderer,C->center.x-C->rad,C->center.x+C->rad,C->center.y,C->border_color);
        vlineColor(renderer,C->center.x,C->center.y-C->rad,C->center.y+C->rad,C->border_color);
    }

    if(!pause)
    {
        if(gravity_on)
        {
            Multiply(&C->acceleration,0);
        }
    }
}

void SetBorderColor(SDL_Color color)
{
    selected_border_color = Color(color.r,color.g,color.b,color.a);
}

void SetBorderForSelected(ball_t* b)
{
    b->border_color = selected_border_color;
}

void RemoveMarker(ball_t* b)
{
    b->border_color = b->color;
}

void GenerateRandomBall(ball_t* b)
{
    int16_t radius = RANDOM(RAND_RADIUS_MIN,RAND_RADIUS_MAX);

    vec2_t center = {
        .x = RANDOM(radius,width-radius),
        .y = RANDOM(radius,heigth-radius)
    };

    int rnd_x = RANDOM(0,1);
    if(!rnd_x) rnd_x = -1;
    int rnd_y = RANDOM(0,1);
    if(!rnd_y) rnd_y = -1;
    vec2_t velocity = {
        .x=RANDOM(1,VELOCITY_MAX) * rnd_x,
        .y=RANDOM(1,VELOCITY_MAX) * rnd_y
    };

    uint32_t color;

    do
    {
        color = Color(RANDOM(0,255),RANDOM(0,255),RANDOM(0,255),0xff);
    }
    while(color == Color(background.r,background.g,background.b,background.a));

    InitBall(b,color,radius,&center,&velocity);
}

void AddBall()
{
    if(balls_num == MAX_NUM_BALLS) return;
    ++balls_num;
    GenerateRandomBall(&ball[balls_num-1]);
}

void DeleteBall(size_t i)
{
    if(!(i < balls_num)) return;
    if(selected_ball_index == i)
        selected_ball_index = BAD_INDEX;
    if(i != balls_num-1)
        ball[i] = ball[balls_num-1];
    --balls_num;
}

void DeleteSelectedBall()
{
    DeleteBall(selected_ball_index);
}

void GenerateBalls()
{
    InitSeed();
    for(size_t i=0;i<balls_num;++i)
    {
        GenerateRandomBall(&ball[i]);
    }
}

void GenerateWindVector(vec2_t* wind)
{
   // wind from west to east
   double random_x = 0.1 * RANDOM(1,WIND_VELOCITY_MAX);
   int direction = RANDOM(0,1);
   if(!direction)
       random_x *= -1; // wind from east to west
   wind->x = random_x;
   wind->y = 0;
}

size_t BigBallContainerIndex(size_t i)
{
    size_t index = BAD_INDEX;
    for(size_t j=0;j<balls_num;++j)
    {
        if(j!=i && ball[i].rad<ball[j].rad &&
                 BallInsideBall(&ball[i],&ball[j]))
        {
            if(index == BAD_INDEX)
                index=j;
            else
                if(ball[index].rad<ball[j].rad)
                    index=j;
        }
    }
    return index;
}

bool SelectBall(int x,int y)
{
    bool selected = false;
    for(size_t i=0;i<balls_num;++i)
        if(PointInsideBall(x,y,&ball[i]))
        {
            selected = true;
            if(selected_ball_index == i)
            {
                // deselect
                selected_ball_index = BAD_INDEX;
                RemoveMarker(&ball[i]);
            }
            else
            {
                // select
                    if(selected_ball_index != BAD_INDEX)
                        RemoveMarker(&ball[selected_ball_index]);
                    selected_ball_index = i;
                    SetBorderForSelected(&ball[i]);
            }
            break;
        }
    return selected;
}

void SelectNextBall()
{
    if(selected_ball_index == BAD_INDEX)
        selected_ball_index = 0;
    else
    {
        ball[selected_ball_index].border_color = ball[selected_ball_index].color;
        selected_ball_index = (selected_ball_index+1)%balls_num;
    }
    ball[selected_ball_index].border_color = selected_border_color;
}

void MoveSelectedBall(int x,int y)
{
    if(selected_ball_index == BAD_INDEX) return;
    ball[selected_ball_index].center = Point(x,y);
}

void ShowStatusLine()
{
    strcpy(status," ");
    char string_num_balls[STRING_LEN_MAX]="";
    UnsignedToString(balls_num,string_num_balls,STRING_LEN_MAX);
    strcat(status,string_num_balls);
    if(balls_num == 1)
        strcat(status," ball");
    else
        strcat(status," balls");
    strcat(status," | swallow: ");
    if(swallow)
        strcat(status,"on");
    else
        strcat(status,"off");
    strcat(status," | friction: ");
    if(friction_on)
        strcat(status,"on");
    else
        strcat(status,"off");
    strcat(status," | gravity: ");
    if(gravity_on)
        strcat(status,"on");
    else
        strcat(status,"off");
    strcat(status," | wind: ");
    if(!wind_on)
        strcat(status,"off");
    else
    {
        if(wind.x<0)
            strcat(status,"<--");
        else
            if(wind.x>0)
                strcat(status,"-->");
            else
                strcat(status,"--");
    }

    FontSetText(&f,status,&white);
    FontWrite(&f,(width-f.width)/2,0);
}

void GoUp(ball_t* b)
{
    if(b->velocity.y == 0) b->velocity.y = b->velocity.x;
    b->velocity.x = 0;
    if(b->velocity.y>0)
    {
        b->velocity.y = -b->velocity.y;
    }
    else
        if(b->velocity.y-VELOCITY_FRACTION > -VELOCITY_MAX)
        {
            b->velocity.y -= VELOCITY_FRACTION;
        }
}

void GoDown(ball_t* b)
{
    if(b->velocity.y == 0) b->velocity.y = b->velocity.x;
    b->velocity.x = 0;
    if(b->velocity.y<0)
    {
        b->velocity.y = -b->velocity.y;
    }
    else
        if(b->velocity.y+VELOCITY_FRACTION < VELOCITY_MAX)
        {
            b->velocity.y += VELOCITY_FRACTION;
        }
}

void GoRight(ball_t* b)
{
    if(b->velocity.x == 0) b->velocity.x = b->velocity.y;
    b->velocity.y = 0;
    if(b->velocity.x<0)
    {
        b->velocity.x = -b->velocity.x;
    }
    else
        if(b->velocity.x+VELOCITY_FRACTION < VELOCITY_MAX)
        {
            b->velocity.x += VELOCITY_FRACTION;
        }
}

void GoLeft(ball_t* b)
{
    if(b->velocity.x == 0) b->velocity.x = b->velocity.y;
    b->velocity.y = 0;
    if(b->velocity.x>0)
    {
        b->velocity.x = -b->velocity.x;
    }
    else
        if(b->velocity.x-VELOCITY_FRACTION > -VELOCITY_MAX)
        {
            b->velocity.x -= VELOCITY_FRACTION;
        }
}
