#ifndef SDL2_BALL_H
#define SDL2_BALL_H

#include<math.h>
#include<stdlib.h>
#include<stdbool.h>
#include<stdint.h>
#include "utils.h"

typedef struct {
    float x,y;
} vec2_t;

typedef struct {
    uint32_t color;
    uint32_t border_color;
    int16_t rad;
    vec2_t center;
    vec2_t velocity;
    vec2_t acceleration;
} ball_t;

vec2_t Point(int16_t x,int16_t y);
#define Vector(x,y) (Point(x,y))
void Add(vec2_t* v1,vec2_t* v2);
void Sub(vec2_t* v1,vec2_t* v2);
void Multiply(vec2_t* v,float f);
float Magnitude(vec2_t* v);
void Normalize(vec2_t* v);
void MoveBallTowardsPoint(ball_t* b,int16_t x,int16_t y);

void InitBall(ball_t* b,uint32_t color,int16_t rad,vec2_t* center,vec2_t* velocity);
void ApplyForce(ball_t* b,vec2_t* v);
void Accelerate(ball_t* b);

double Distance(ball_t* b1,ball_t* b2);
bool PointInsideBall(int x,int y,ball_t* C);
bool BallsTouchExternally(ball_t* b1,ball_t* b2);
bool BallsTouchInternally(ball_t* b1,ball_t* b2);
bool BallsIntersect2Points(ball_t* b1,ball_t* b2);
bool BallInsideBall(ball_t* b1,ball_t* b2);

#ifdef SDL2_BALL_IMPLEMENTATION

vec2_t Point(int16_t x,int16_t y){
    vec2_t P = {.x=x, .y=y};
    return P;
}

void Add(vec2_t* v1,vec2_t* v2)
{
    v1->x += v2->x;
    v1->y += v2->y;
}

void Sub(vec2_t* v1,vec2_t* v2)
{
    v1->x -= v2->x;
    v1->y -= v2->y;
}

void Multiply(vec2_t* v,float f)
{
    v->x *= f;
    v->y *= f;
}

float Magnitude(vec2_t* v)
{
    return sqrt(SQR(v->x)+SQR(v->y));
}

void Normalize(vec2_t* v)
{
    float m = Magnitude(v);
    if(m == 0) return;
    v->x /= m;
    v->y /= m;
}

void MoveBallTowardsPoint(ball_t* b,int16_t x,int16_t y)
{
    float m = Magnitude(&b->velocity);
    vec2_t direction = Vector(x - b->center.x,y - b->center.y);
    Normalize(&direction);
    Multiply(&direction,m);
    b->velocity = direction;
}

void InitBall(ball_t* b,uint32_t color,int16_t rad,vec2_t* center,vec2_t* velocity)
{
    b->color = color;
    b->border_color = color;
    b->rad = rad;
    b->center = *center;
    b->velocity = *velocity;
    b->acceleration = Vector(0,0);
}

void ApplyForce(ball_t* b,vec2_t* v)
{
    Add(&b->acceleration,v);
}

void Accelerate(ball_t* b)
{
    Add(&b->velocity,&b->acceleration);
    b->acceleration=Vector(0,0);
}

double Distance(ball_t* b1,ball_t* b2)
{
    return sqrt(SQR(b1->center.x - b2->center.x) +
             SQR(b1->center.y - b2->center.y));
}

bool PointInsideBall(int x,int y,ball_t* C)
{
    return sqrt(SQR(C->center.x - x)+SQR(C->center.y - y)) <= C->rad;
}

bool BallsTouchExternally(ball_t* b1,ball_t* b2)
{
    return Distance(b1,b2) == b1->rad + b2->rad;
}

bool BallsTouchInternally(ball_t* b1,ball_t* b2)
{
    return Distance(b1,b2) == abs(b1->rad - b2->rad);
}

bool BallsIntersect2Points(ball_t* b1,ball_t* b2)
{
    double d = Distance(b1,b2);
    return (abs(b1->rad - b2->rad) < d) && (d < b1->rad + b2->rad);
}

bool BallInsideBall(ball_t* b1,ball_t* b2)
{
    return Distance(b1,b2) < abs(b1->rad - b2->rad);
}

#endif //SDL2_BALL_IMPLEMENTATION

#endif //SDL2_BALL_H
