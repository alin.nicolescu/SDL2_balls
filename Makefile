CC = cc

# CC_FLAGS = -Wall -Wextra -ggdb
CC_FLAGS = -Wall -Wextra
SDL2_FLAGS = `sdl2-config --cflags`
CC_LIBS = -lm
SDL2_LIBS = `sdl2-config --libs` -lSDL2_gfx -lSDL2_ttf

FLAGS = $(CC_FLAGS) $(SDL2_FLAGS)
LIBS = $(SDL2_LIBS) $(CC_LIBS)

TARGET = balls
OBJ = main.o SDL2_template.o SDL2_fonts.o SDL2_ball.o utils.o

$(TARGET): $(OBJ)
	$(CC) -o $(TARGET) $(FLAGS) $(OBJ) $(LIBS) 

SDL2_template.o : SDL2_template.h
	$(CC) -DSDL2_TEMPLATE_IMPLEMENTATION $(FLAGS) -x c -c SDL2_template.h

SDL2_fonts.o : SDL2_fonts.h
	$(CC) -DSDL2_FONTS_IMPLEMENTATION $(FLAGS) -x c -c SDL2_fonts.h

SDL2_ball.o : SDL2_ball.h
	$(CC) -DSDL2_BALL_IMPLEMENTATION $(FLAGS) -x c -c SDL2_ball.h

utils.o : utils.h
	$(CC) -DUTILS_IMPLEMENTATION $(CC_FLAGS) -x c -c utils.h

main.o: main.c
	$(CC) $(FLAGS) -c main.c

.PHONY: clean
clean:
	rm -fv $(TARGET) $(OBJ)
