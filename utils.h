#ifndef UTILS_H
#define UTILS_H

#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<time.h>

#define SQR(a) ((a)*(a))
#define MAX(a,b) (a>b? a:b)
#define MIN(a,b) (a<b? a:b)
#define RANDOM(a,b) ((a)+rand()%((b+1)-(a)))

void InitSeed(void);
size_t NumberOfDigits(long int n);
void UnsignedToString(unsigned n,char* s,size_t string_size);
uint32_t Color(uint8_t r,uint8_t g,uint8_t b,uint8_t a);

#ifdef UTILS_IMPLEMENTATION

void InitSeed(void)
{
    srand(time(NULL));
}

size_t NumberOfDigits(long int n)
{
    if(n<0) n=-n;
    size_t nr = 0;
    do
    {
        ++nr;
        n/=10;
    }
    while(!n);

    return nr;
}

void UnsignedToString(unsigned n,char* s,size_t string_size)
{
    size_t nr = 0;
    do
    {
        if(nr == string_size-1)
        {
            fprintf(stderr,"[ERROR] Number to string conversion: number too long\n");
            exit(EXIT_FAILURE);
        }
        s[nr++] = (char) ('0' + n%10);
        n/=10;
    }
    while(n);
    s[nr]='\0';
    for(size_t i=0;i<nr/2;++i)
    {
        char c = s[i];
        s[i] = s[nr-i-1];
        s[nr-i-1] = c;
    }
}

uint32_t Color(uint8_t r,uint8_t g,uint8_t b,uint8_t a)
{
    uint32_t c = (r << 24) + (g<<16) + (b<<8) + a;
    return c;
}

#endif // UTILS_IMPLEMENTATION

#endif // UTILS_H
